function board () {
    var a = '';
    for (var i = 1; i <= 8; i++) {
        for (var j = 1; j <= 8; j++) {
            if ((i % 2 === 0 && j % 2 === 0) || (i % 2 != 0 && j % 2 != 0)) {
                a += '  ';
            } else {
                a += '██';
            }
        }
     a += '\n';
    }
    return a;
}

console.log(board());